from django.conf.urls import url
from base import views

__author__ = 'ahmetdal'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^detail/(?P<pk>\d+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^results/(?P<pk>\d+)/$', views.ResultsView.as_view(), name='results'),
    url(r'^vote/(?P<question_id>\d+)/$', views.VoteView.as_view(), name='vote'),
    url(r'^question/$', views.QuestionView.as_view(), name='question'),
    url(r'^choice/(?P<question_id>\d+)/$', views.ChoiceView.as_view(), name='choice'),

]
