# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0003_question_oyle_bi_kolon'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='oyle_bi_kolon',
        ),
    ]
