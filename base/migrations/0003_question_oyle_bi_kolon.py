# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20150617_0737'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='oyle_bi_kolon',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 17, 7, 40, 58, 826602, tzinfo=utc), verbose_name=b''),
            preserve_default=False,
        ),
    ]
