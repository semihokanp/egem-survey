from django.contrib import admin

# Register your models here.
from base.models import Question, Choice


class MahmutInline(admin.StackedInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    list_display = ['question_text', 'pub_date', 'mahmutt']
    list_filter = ['question_text', 'pub_date']
    search_fields = ['question_text']
    fieldsets = [
        (None, {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [MahmutInline]

    def mahmutt(self, instance):
        return instance.mahmut.all()


class MahmutAdmin(admin.ModelAdmin):
    list_display = ['question', 'choice_text', 'votes']
    list_filter = ['question', 'choice_text', 'votes']


admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, MahmutAdmin)
